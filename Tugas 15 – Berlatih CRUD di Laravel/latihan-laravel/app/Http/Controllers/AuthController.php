<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('page.register');
    }

    public function welc(Request $request)
    {
        //dd($request ->all());
        $namaDepan = $request['depan'];
        $namaBelakang = $request['belakang'];
        
        return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
    
}
