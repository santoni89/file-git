<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }
    public function store(request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus di isi',
            'umur.required' => 'Umur harus di isi',
            'umur.numeric' => 'Umur harus di Angka',
            'bio.required' => 'Bio harus di isi',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $data = DB::table('cast')->get();
        //dd($data);
        return view('cast.tampil', ['data' => $data]);
    }
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        //dd($cast);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus di isi',
            'umur.required' => 'Umur harus di isi',
            'umur.numeric' => 'Umur harus di Angka',
            'bio.required' => 'Bio harus di isi',
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }

    public function destroy($id, Request $request)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }

}
