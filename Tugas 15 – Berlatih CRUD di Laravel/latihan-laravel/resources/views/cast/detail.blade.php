@extends('layouts.master')

@section('tittle')
Halaman Detail Cast
@endsection

@section('content')

<h1>Nama: {{$cast->nama}}</h1>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>

<br>
<br>
<br>
<a href="/cast" class="btn btn-secondary" role="button">Back</a>
@endsection