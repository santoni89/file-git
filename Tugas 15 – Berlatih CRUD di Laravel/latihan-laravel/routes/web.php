<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'regis']);
Route::post('/welcome', [AuthController::class, 'welc']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.data-table');
});

//CRUD CAST
//( Method boleh sama, asal link '/' berbeda)
//Create Data
//Mengarah ke Form Tambah Data Cast
Route::get('/cast/create',[CastController::class, 'create']);

//Menyimpan Data ke Table DB Cast
Route::post('/cast', [CastController::class, 'store']);

//Membaca Data dari  Table DB Cast
Route::get('/cast', [CastController::class, 'index']);

//Detail Data Cast ambil berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Data (Tombol Edit dan edit isi)
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//Update Data (Menginput data yang sudah di edit)
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Data dari Table DB Cast
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
